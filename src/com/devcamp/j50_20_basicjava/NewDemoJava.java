package com.devcamp.j50_20_basicjava;

public class NewDemoJava {
    public static void main(String[] args) {
        System.out.println("New Demo Java Class");

        NewDemoJava.name(30, "Nguyen Van Nam");//goi phuong thuc tinh: dung truc tiep Class.TenPhuongThuc

        /**
         * Goi phuong thuc thuong: dung qua doi tuong (khoi tao => goi phuong thuc)
         */
        NewDemoJava demo = new NewDemoJava();
        demo.name("Tran Van Thang");
        String strTmp = demo.name("Nguyen Van Thuan", 35);
        System.out.println(strTmp);
    }

    public void name(String strName) {
        System.out.println(strName);  
    }

    public String name(String strName, int age) {
        String strResult = "My name is " + strName + ", my age is " + age;
        return strResult;  
    }

    public static void name(int age, String strName) {
        String strResult = "My name is " + strName + ", my age is " + age;
        System.out.println(strResult);
    }
}
